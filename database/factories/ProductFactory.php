<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->streetName,
        'description' => '"'.$faker->realText(3000).'"',
        'price' => $faker->randomFloat(2,0,100000),
        'active' => $faker->boolean,
        'user_id' => \App\User::inRandomOrder()->first()->id,
    ];
});
