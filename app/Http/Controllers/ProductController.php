<?php

namespace App\Http\Controllers;

use App\Product;
use App\User;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    /*
     * Instantiate a new ProductController instance.
     */
    public function __construct()
    {
        $this->middleware('apikey');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $products = User::where('api_key',$request->api_key)->firstOrFail()->products()->get();
            return response()->json($products,200);
        } catch(\Exception $e){
            if($products->count() == 0)
            return response()->json(['message' => 'No products found.'],401);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $entry = $request->only('name', 'description', 'price', 'active');

        try{
            $user = User::where('api_key',$request->api_key)->firstOrFail();
        } catch(\Exception $e){
            return response()->json(['message' => 'API Key is not valid'],401);
        }

        $product = new Product($entry);
        $product->user()->associate($user);
        $product->save();

        return response()->json([$product],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product, Request $request)
    {
        try {
            $user_product = User::where('api_key',$request->api_key)->firstOrFail()->products()->withTrashed()->findOrFail($product->id);
            if($user_product->trashed())
                response()->json(['message' => 'Product has been deleted from the database.'],404);
            return response()->json($user_product,200);
        } catch(\Exception $e){
            return response()->json(['message' => 'Product not fount in this user storage.','product_id' => $product->id],404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
         $entry = $request->only('name', 'description', 'price', 'active');
         try {
            $user_product = User::where('api_key',$request->api_key)->firstOrFail()->products()->findOrFail($product->id);            
            $user_product->update($entry);
            return response()->json([$user_product],200);
        } catch(\Exception $e){
            return response()->json(['message' => 'Product not fount in this user storage.','product_id' => $product->id],404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Product $product)
    {
        try {
            $user_product = User::where('api_key',$request->api_key)->firstOrFail()->products()->findOrFail($product->id);            
            $user_product->delete();
            return response()->json(['message' => 'Product with '.$product->id.' was succesfully deleted.'],200);
        } catch(\Exception $e){
            return response()->json(['message' => 'Product not fount in this user storage.','product_id' => $product->id,'Exception'=>$e],404);
        }
    }
}
