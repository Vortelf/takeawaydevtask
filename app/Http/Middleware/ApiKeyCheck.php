<?php

namespace App\Http\Middleware;

use Closure;
use App\User;

class ApiKeyCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        if(empty($request->api_key))
            return response()->json(['message' => 'API Key not provided.', 401]);
        try{
            if($user = User::where('api_key',$request->api_key)->firstOrFail())
                return $next($request);
            //return response()->json(['key'=>$request->api_key],500);
         } catch(\ModelNotFoundException $e){
            return response()->json(['message' => 'API Key is not valid', 401]);
        }

        return $next($request);
    }
}
